using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestoEbstvo : MonoBehaviour
{
    private void Start()
    {
        WeaponEbala w = new WeaponEbala();
        Ammo a = new Ammo();

        List<IItem<IStats>> items = new List<IItem<IStats>>
        {
            (IItem<IStats>)w,
            (IItem<IStats>)a
        };

        print(items[1].Stats);
        //print(items[0].)

    }
}

public class WeaponEbala : IItem<WeaponStats>
{
    public WeaponStats Stats { get; set; }
}

public class Ammo : IItem<AmmoStats>
{
    public AmmoStats Stats { get; set; }
}

public class Item<StatsType> : IItem<StatsType>
{
    public StatsType Stats { get; set; }
}

public interface IItem<T> 
{
    public T Stats { get; set; }
}

public struct WeaponStats : IStats
{
    public string sosi => "sosi";
    public IStats GetStats()
    {
        return this;
    }
}

public struct AmmoStats : IStats
{
    public string ebi => "ebi";

    public IStats GetStats()
    {
        return this;
    }
}

public interface IStats 
{
    //public IStats GetStats();
}
