﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class StickmanController : MonoBehaviour
{
    public Muscle[] muscles;
    public Transform[] bones;

    [SerializeField]
    private Animator animator;

    [SerializeField]
    Vector2 forceVector;
    [SerializeField]
    private float moveDelay;
    [SerializeField]
    private Rigidbody2D rightLeg;
    [SerializeField]
    private Rigidbody2D leftLeg;
    [SerializeField]
    private Rigidbody2D rightArm;
    [SerializeField]
    private Rigidbody2D leftArm;
    [SerializeField]
    private int musclePower = 100;

    [Space]

    [SerializeField]
    private Transform rightHandPoint;
    [SerializeField]
    private Transform leftHandPoint;

    public Rigidbody2D RightArm => rightArm;
    public Rigidbody2D LeftArm => leftArm;
    public Transform RightHand => rightHandPoint;
    public Transform LeftHand => leftHandPoint;

    [SerializeField] Muscle[] armParts = new Muscle[4];

    float moveDelayPointer;

    private void Start()
    {
        armParts = muscles.ToList().Where(m => m.bone.name.Contains("Arm")).ToArray();
    }

    private void FixedUpdate()
    {
        //if (!transform.root.GetComponent<Photon.Pun.PhotonView>().IsMine)
        //    return;

        if (!MultiplayerManager.IsMaster)
            return;

        foreach (Muscle muscle in muscles)
        {
            muscle.ActivateMuscle();
        }
    }

    public void Stun()
    {
        musclePower = 0;
        Xyi();
    }

    public void IgnoreCollision(Collider2D collider, bool ignore = true)
    {
        foreach (var muscle in muscles)
        {
            Physics2D.IgnoreCollision(muscle.bone.GetComponent<Collider2D>(), collider, ignore);
        }
    }

    public void SetForceToArms(int force)
    {
        armParts.ToList().ForEach(a => a.force = force);
    }

    public void MoveRight()
    {
        while (Time.time > moveDelayPointer)
        {
            Step1(1);
            Step2(1, 0.085f);
            moveDelayPointer = Time.time + moveDelay;
        }
    }

    public void MoveLeft()
    {
        while (Time.time > moveDelayPointer)
        {
            Step1(-1);
            Step2(-1, 0.085f);
            moveDelayPointer = Time.time + moveDelay;
        }
    }

    public void Jump(int force)
    {
        rightLeg.AddForce(3 * Vector2.up * force, ForceMode2D.Impulse);
        leftLeg.AddForce(3 * Vector2.up  * force, ForceMode2D.Impulse);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            Xyi();
        }
    }

    void Step1(int dir, float delay = 0)
    {
        StartCoroutine(DelayInvoke());

        IEnumerator DelayInvoke()
        {
            yield return new WaitForSeconds(delay);

            rightLeg.AddForce(forceVector * dir, ForceMode2D.Impulse);
            leftLeg.AddForce(forceVector * -0.5f * dir, ForceMode2D.Impulse);
        }
        
    }

    void Step2(int dir, float delay = 0)
    {
        StartCoroutine(DelayInvoke());

        IEnumerator DelayInvoke()
        {
            yield return new WaitForSeconds(delay);

            rightLeg.AddForce(forceVector * -0.5f * dir, ForceMode2D.Impulse);
            leftLeg.AddForce(forceVector * dir, ForceMode2D.Impulse);
        }
    }

    void Xyi()
    {
        foreach (Muscle muscle in muscles)
        {
            muscle.force = musclePower;
        }
    }

}

[System.Serializable]
public class Muscle
{
    public Rigidbody2D bone;
    public Transform boneAnimated;
    public float restRotation;
    public float force;


    public void ActivateMuscle()
    {

        restRotation = boneAnimated.rotation.eulerAngles.z;
                
        bone.MoveRotation(Mathf.LerpAngle(bone.rotation, restRotation, force * Time.deltaTime));    
    }
}
