using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    bool ebalaTesto;

    public static bool DESTROY_BULLET;

    [SerializeField]
    Vector3 spawnPos;
    [SerializeField]
    private Player playerAIPrefab;

    private IEnumerator Start()
    {
        DESTROY_BULLET = ebalaTesto;

        yield return new WaitForSeconds(1.5f);

        if (MultiplayerManager.IsMaster)
        {
            PhotonNetwork.Instantiate(playerAIPrefab.name, spawnPos, Quaternion.identity);
            
        }
    }
}
