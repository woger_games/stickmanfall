using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerBehaviour : MonoBehaviour
{
    Player player;
    PhotonView view;

    private void Start()
    {
        player = GetComponent<Player>();
        view = GetComponent<PhotonView>();

        if (view.IsMine)
        {
            EventsHolder.playerSpawnedMine?.Invoke(player);

            EventsHolder.leftJoystickMoved.AddListener(Move);
            EventsHolder.rightJoystickMoved.AddListener(Attack);
            EventsHolder.rightJoystickUp.AddListener(RightJoystick_Uped);
            EventsHolder.jumpClicked.AddListener(Jump);
        }
    }

    private void Update()
    {
        if (!view.IsMine)
            return;

        if (Input.GetKey(KeyCode.D))
        {
            EventsHolder.leftJoystickMoved?.Invoke(new Vector2(1, 0));
        }

        if (Input.GetKey(KeyCode.A))
        {
            EventsHolder.leftJoystickMoved?.Invoke(new Vector2(-1, 0));
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            EventsHolder.jumpClicked?.Invoke();
        }
    }

    public void Move(Vector2 dir)
    {
        player.Move(dir);
    }

    private void Attack(Vector2 dir)
    {
        player.Attack(dir);
    }

    public void RightJoystick_Uped()
    {
        player.Idle();
    }

    public void Jump()
    {
        player.Jump();
    }
}
