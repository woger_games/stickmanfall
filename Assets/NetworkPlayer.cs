using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using System.Linq;
using System;

public class NetworkPlayer : MonoBehaviourPunCallbacks
{
    Player player;

    int masterActorNumber = 1;

    private void Awake()
    {
        player = GetComponent<Player>();
    }

    private void Start()
    {
        // --- Input ---
        EventsHolder.leftJoystickMoved.AddListener(LeftJoystick_Moved);
        EventsHolder.rightJoystickMoved.AddListener(RightJoystick_Moved);
        EventsHolder.rightJoystickUp.AddListener(RightJoystick_Uped);
        // -------------
        EventsHolder.weaponPicked.AddListener(Weapon_Picked);
        EventsHolder.weaponThrowed.AddListener(Weapon_Throwed);

        
        photonView.RegisterMethod<DirectionNetworkData>(EventCode.PlayerMove, ClientPlayer_Moved);
        photonView.RegisterMethod<DirectionNetworkData>(EventCode.WeaponRotate, ClientPlayer_WeaponRotate);
        photonView.RegisterMethod<WeaponDependencyNetworkData>(EventCode.WeaponPicked, NetworkWeapon_Picked);
        photonView.RegisterMethod<WeaponDependencyNetworkData>(EventCode.WeaponThrowed, NetworkWeapon_Throwed);
        photonView.RegisterMethod<int>(EventCode.RightJoystickUp, Client_RightJoystickUped);
    }

    private void NetworkWeapon_Throwed(WeaponDependencyNetworkData pickedData)
    {
        if (photonView.ViewID == pickedData.playerViewID)
        {
            var weapons = FindObjectsOfType<Weapon>();
            foreach (var weapon in weapons)
            {
                if (pickedData.weaponViewID == weapon.GetComponent<PhotonView>().ViewID)
                {
                    player.ThrowWeapon(weapon);
                }
            }
        }
    }

    private void Weapon_Throwed(Player player, Weapon weapon)
    {
        var data = new WeaponDependencyNetworkData
        {
            playerViewID = player.GetComponent<PhotonView>().ViewID,
            weaponViewID = weapon.GetComponent<PhotonView>().ViewID,
        };

        photonView.RaiseEvent(EventCode.WeaponThrowed, data);
    }

    private void NetworkWeapon_Picked(WeaponDependencyNetworkData pickedData)
    {
        if(photonView.ViewID == pickedData.playerViewID)
        {
            var weapons = FindObjectsOfType<Weapon>();
            foreach (var weapon in weapons)
            {
                if (pickedData.weaponViewID == weapon.GetComponent<PhotonView>().ViewID)
                {
                    player.TakeWeapon(weapon);
                }
            }
        }
    }

    private void Weapon_Picked(Player player, Weapon weapon)
    {
        var data = new WeaponDependencyNetworkData
        {
            playerViewID = player.GetComponent<PhotonView>().ViewID,
            weaponViewID = weapon.GetComponent<PhotonView>().ViewID,
        };

        photonView.RaiseEvent(EventCode.WeaponPicked, data);
    }

    private void RightJoystick_Moved(Vector2 dir)
    {
        if (PhotonNetwork.IsMasterClient)
        {

        }
        else
        {
            var data = new DirectionNetworkData { value = dir, viewID = photonView.ViewID };
            photonView.RaiseEvent(EventCode.WeaponRotate, data, masterActorNumber);
        }
    }

    private void RightJoystick_Uped()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            photonView.RaiseEvent(EventCode.RightJoystickUp, photonView.ViewID, masterActorNumber);
        }
    }

    private void LeftJoystick_Moved(Vector2 dir)
    {
        if (PhotonNetwork.IsMasterClient)
        {

        }
        else
        {
            var data = new DirectionNetworkData { value = dir, viewID = photonView.ViewID };
            photonView.RaiseEvent(EventCode.PlayerMove, data, masterActorNumber);
        } 
    }

    private void ClientPlayer_WeaponRotate(DirectionNetworkData dir)
    {
        if (dir.viewID == photonView.ViewID)
        {
            player.Weapons.ForEach(w => w.SetRestRotationByDir(dir.value));
            player.Handler.ChooseAnimation(dir.value);
        }
    }

    private void ClientPlayer_Moved(DirectionNetworkData dir)
    {
        if(dir.viewID == photonView.ViewID)
        {
            player.Move(dir.value);
        }
    }

    private void Client_RightJoystickUped(int viewID)
    {
        if(viewID == photonView.ViewID)
        {
            player.Idle();
        }
    }
}
