using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;

public class NetworkProjectileHandler : MonoBehaviour, IPunObservable
{
    [SerializeField]
    int forcePos = 30;
    [SerializeField]
    float lifetime = 0.5f;
    [SerializeField] 
    List<Projectile> projectiles;
    [SerializeField]
    List<Vector2> velocities;
    [SerializeField]
    List<Vector2> positions;
    [SerializeField]
    List<bool> achieveds;

    Projectile synchronizable;
    Vector2 syncPosition;
    Vector2 syncVelocity;

    int currentIdx = 0;
   

    private void Awake()
    {
        projectiles = new List<Projectile>();
        velocities = new List<Vector2>();
        positions = new List<Vector2>();
        achieveds = new List<bool>();
    }

    private void Start()
    {
        EventsHolder.networkProjectileInited.AddListener(Projectile_Inited);
    }

    private void FixedUpdate()
    {
        if (PhotonNetwork.IsMasterClient)
            return;

        for (int i = 0; i < projectiles.Count; i++)
        {
            if (projectiles[i].Lifetime < lifetime || positions[i] == Vector2.zero)
                continue;

            var body = projectiles[i].Body;

            projectiles[i].Body.angularVelocity = 0;
            projectiles[i].Body.velocity = velocities[i];

            var distance = Vector2.Distance(body.position, positions[i]);

            if (achieveds[i])
            {
                if (body.velocity.magnitude < 0.03f)
                {
                    body.MovePosition(Vector2.Lerp(body.position, positions[i], forcePos * Time.deltaTime));
                }

                if (distance > 0.1f)
                {
                    achieveds[i] = false;
                }
            }
            else
            {
                body.MovePosition(Vector2.Lerp(body.position, positions[i], forcePos * Time.deltaTime));

                if(distance < 0.01f)
                {
                    achieveds[i] = true;
                }
            }

        }

    }

    private void Projectile_Inited(Projectile projectile)
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            foreach (var p in projectiles)
            {
                //Physics2D.IgnoreCollision(projectile.GetComponent<Collider2D>(), p.GetComponent<Collider2D>());
            }

            projectile.Body.gravityScale = 0;
            //projectile.GetComponent<Collider2D>().isTrigger = true;
            velocities.Add(projectile.Body.velocity);
            positions.Add(Vector2.zero);
            achieveds.Add(false);
        }
        
        projectiles.Add(projectile);
        

        //StartCoroutine(Delay());

        //IEnumerator Delay()
        //{
        //    yield return null;

        //    velocities.Add(projectile.Body.velocity);
        //}
    }

    /// <summary>
    /// ������ ������, ������ ��� ����������� ��� �� ��� ��������,
    /// � ������, ��� ��� ������ ������ ������, ��� � �����
    /// </summary>
    /// <param name="stream"></param>
    /// <param name="info"></param>
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (projectiles.Count == 0)
            return;

        if (stream.IsWriting)
        {
            stream.SendNext(projectiles[currentIdx].Body.position);
            stream.SendNext(projectiles[currentIdx].Body.velocity);
            stream.SendNext(projectiles[currentIdx].ID.Value);

            currentIdx++;
            if (currentIdx >= projectiles.Count)
                currentIdx = 0;
        }
        else
        {
            syncPosition = (Vector2)stream.ReceiveNext();
            syncVelocity = (Vector2)stream.ReceiveNext();
            var projectileID = (int)stream.ReceiveNext();

            synchronizable = projectiles.Find(p => p.ID == projectileID);

            int idx = projectiles.IndexOf(synchronizable);
            velocities[idx] = syncVelocity;
            positions[idx] = syncPosition;
        }
    }
}
