using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine;
using System.Linq;

public class NetworkRigidbodyesHandler : MonoBehaviour, IPunObservable
{
    [SerializeField]
    int forcePos = 30;
    [SerializeField]
    int forceRot = 30;
    [SerializeField]
    List<Rigidbody2D> bodyes;
    [SerializeField]
    List<Vector2> positions;
    [SerializeField]
    List<float> rotations;


    private void Awake()
    {
        bodyes = new List<Rigidbody2D>();
        positions = new List<Vector2>();
        rotations = new List<float>();
    }

    private void Start()
    {
        EventsHolder.networkBodyInited.AddListener(Body_Inited);
    }

    private void FixedUpdate()
    {
        if (positions.Count < bodyes.Count)
            return;

        int idx = 0;

        foreach (var body in bodyes)
        {
            if (body.bodyType == RigidbodyType2D.Dynamic)
            {
                body.MovePosition(Vector2.Lerp(body.position, positions[idx], forcePos * Time.deltaTime));
                body.MoveRotation(Mathf.LerpAngle(body.rotation, rotations[idx], forceRot * Time.deltaTime));
            }

            idx++;
            
        }
    }

    private void Body_Inited(Rigidbody2D body)
    {
        bodyes.Add(body);

        bodyes = bodyes.OrderBy(b => b.GetComponent<PhotonView>().ViewID).ToList();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        positions.Clear();
        rotations.Clear();

        foreach (var body in bodyes)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(body.position);
                stream.SendNext(body.rotation);
            }
            else
            {
                if (stream.Count < bodyes.Count)
                    return;

                var pos = (Vector2)stream.ReceiveNext();
                positions.Add(pos);
                
                var rot = (float)stream.ReceiveNext();
                rotations.Add(rot);
                
            }
        }
    }
}
