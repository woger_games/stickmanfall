using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;

public class NetworkLongRange : MonoBehaviourPunCallbacks
{
    LongRange weapon;

    private void Awake()
    {
        weapon = GetComponent<LongRange>();
    }

    private void Start()
    {
        weapon.onAttack += Attacked;
        weapon.ViewID = photonView.ViewID;

        photonView.RegisterMethod<int>(EventCode.Attack, Network_Attacked, true);
    }

    private void Network_Attacked(int fullID)
    {
        string data = $"{fullID}";
        var strViewID = $"{photonView.ViewID}";

        var receivedWeaponID = int.Parse(data.Substring(0, strViewID.Length));

        if(receivedWeaponID == photonView.ViewID)
        {
            var projectileID = int.Parse(data.Substring(strViewID.Length));
            weapon.Shot(projectileID);
        }
    }

    private void Attacked(Projectile p)
    {
        string fullID = $"{weapon.ViewID}{p.ID}";

        var data = int.Parse(fullID);
        
        photonView.RaiseEventAll(EventCode.Attack, data);
        //Debug.LogError(data);
    }
}
