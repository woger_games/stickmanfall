﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;

using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class Player : MonoBehaviour
{
    [SerializeField]
    private Transform hip;
    [SerializeField]
    private StickmanController ragdoll;
    [SerializeField]
    private AnimationHandler animationHandler;

    [Space]

    [SerializeField]
    private SpriteRenderer leftArmTop;

    public int totalPower;
    public bool isLongRangeAttack;

    public Transform Hip => hip;
    public StickmanController Ragdoll => ragdoll;
    public AnimationHandler Handler => animationHandler;
    public List<Weapon> Weapons { get; private set; }
    public ArmJointSet AvailableArm { get; private set; }
    public WeaponSet WeaponSet { get; set; }

    [SerializeField]
    WeaponSet ururu;
    [SerializeField]
    float groundedRayLength = 1f;

    private void Awake()
    {
        Weapons = new List<Weapon>();

        foreach (var item in GetComponentsInChildren<CollisionHandler>())
        {
            item.Player = this;
        }
    }

    private void Start()
    {
        EventsHolder.playerSpawnedAny?.Invoke(this);

        CalcultePower();
    }


    private void Update() 
    {
        animationHandler.transform.position = hip.position;        

        ururu = WeaponSet;
    }

    public void Move(Vector2 dir)
    {
        if(dir.x > 0)
        {
            ragdoll.MoveRight();
            animationHandler.SetDirectionView(1);
            Weapons.ForEach(w => w.Rotate(1));
        }
        else
        {
            ragdoll.MoveLeft();
            animationHandler.SetDirectionView(-1);
            Weapons.ForEach(w => w.Rotate(-1));
        }
    }

    public void Jump()
    {
        bool jumpAvailable = false;
        var hits = Physics2D.RaycastAll(hip.position, Vector2.down, groundedRayLength);
        foreach (var hit in hits)
        {
            var foundBody = ragdoll.muscles.ToList().Find(b => b.bone.gameObject == hit.collider.gameObject)?.bone.gameObject;
            if (!foundBody)
            {
                jumpAvailable = true;
                break;
            }
        }

        if (jumpAvailable)
        {
            ragdoll.Jump(totalPower);
        }
    }

    public void Attack(Vector2 dir)
    {
        if (Weapons.Count == 0)
            return;

        var magnitude = dir.magnitude;

        if (WeaponSet == WeaponSet.MeleeAndLongRange)
        {
            if (magnitude > Constants.Melee_And_Long_Range_Attack_Range_Thresold
             && magnitude < Constants.Melee_And_Long_Range_Attack_Melee_Thresold)
            {
                isLongRangeAttack = true;
                Weapons.ForEach(w => w.Attack(dir));
            }
            else if(isLongRangeAttack)
            {
                isLongRangeAttack = false;
                // Возможно понадобится делать это только для лонг рендж
                Idle();
            }
        }
        else
        {
            if (magnitude > 0.7f)
                Weapons.ForEach(w => w.Attack(dir));
        }

        animationHandler.ChooseAnimation(dir);
    }


    public void Idle()
    {
        isLongRangeAttack = false;
        animationHandler.Idle();
        Weapons.ForEach(w => w.Idle());
    }

    public void TakeWeapon(Weapon weapon)
    {
        CheckWeaponAndArmAvailable(weapon);

        ragdoll.IgnoreCollision(weapon.Collider);
        ragdoll.SetForceToArms(weapon.RequaredForceArms);
        Weapons.Add(weapon);
        weapon.SetOwner(this);
        animationHandler.ChooseWeaponPose(WeaponSet);

        EventsHolder.weaponPicked?.Invoke(this, weapon);
        
        if(weapon as Throwen)
        {
            (weapon as Throwen).captured += ThrowenWeapon_Captured;
        }
    }

    private void ThrowenWeapon_Captured(Throwen throwen)
    {
        if(totalPower > throwen.powerForGotUp)
        {
            animationHandler.GotUp();
            leftArmTop.sortingOrder = -1;
        }
    }

    public void ThrowWeapon(Weapon weapon)
    {
        DOTween.Sequence().AppendInterval(0.88f).OnComplete(ignoreCollision);

        void ignoreCollision() => ragdoll.IgnoreCollision(weapon.Collider, false);

        ragdoll.SetForceToArms(Constants.FORCE_ARMS);

        weapon.BreakJoints();

        EventsHolder.weaponThrowed?.Invoke(this, weapon);

        // --- Ебучий костыль ---
        if (weapon as Throwen)
        {
            DOTween.Sequence().AppendInterval(0.01f).OnComplete(() => Weapons.Remove(weapon));
        }
        else
        {
            Weapons.Remove(weapon);
        }
        // ----------------------

        if (weapon as Throwen)
        {
            (weapon as Throwen).captured -= ThrowenWeapon_Captured;
        }

    }
    
    /// <summary>
    /// А так же назначение типа набора оружия - WeaponSet
    /// </summary>
    /// <param name="newWeapon"></param>
    private void CheckWeaponAndArmAvailable(Weapon newWeapon)
    {
        if (newWeapon is Melee)
        {
            WeaponSet = WeaponSet.MeleeOne;

            if (newWeapon.JointSecond.hilt)
            {
                WeaponSet = WeaponSet.MeleeTwoHanded;
            }
        }
        else if(newWeapon is LongRange)
        {
            WeaponSet = WeaponSet.LongRangeOne;

            if (newWeapon.JointSecond.hilt)
            {
                WeaponSet = WeaponSet.LongRangeTwoHanded;
            }
        }
        else
        {
            WeaponSet = WeaponSet.Throwen;
        }

        if (Weapons.Any())
        {
            var currentWeapon = Weapons.First();
            if (currentWeapon.JointSecond.hilt || newWeapon.JointSecond.hilt || Weapons.Count > 1)
            {
                var length = Weapons.Count;
                for (int i = 0; i < length; i++) ThrowWeapon(Weapons.First());
                RightJointAvailable();
            }
            else
            {
                AvailableArm = new ArmJointSet 
                { 
                    arm = ragdoll.LeftArm, 
                    weaponHolder = animationHandler.LeftWeapon,
                    hand = ragdoll.LeftHand,
                };

                if(currentWeapon is Melee && newWeapon is Melee)
                {
                    WeaponSet = WeaponSet.MeleeTwo;
                }
                else if(currentWeapon is LongRange && newWeapon is LongRange)
                {
                    WeaponSet = WeaponSet.LongRangeTwo;
                }
                else
                {
                    WeaponSet = WeaponSet.MeleeAndLongRange;

                    if(currentWeapon is LongRange)
                    {
                        CastlingWeapons(newWeapon, currentWeapon);
                    }
                }
            }
        }
        else
        {
            RightJointAvailable();
        }

        void RightJointAvailable()
        {
            AvailableArm = new ArmJointSet
            {
                arm = ragdoll.RightArm,
                weaponHolder = animationHandler.RightWeapon,
                hand = ragdoll.RightHand,
            };
        }

        EventsHolder.weaponSetInited.Invoke(WeaponSet);
    }

    private void CastlingWeapons(Weapon melle, Weapon longRange)
    {
        StartCoroutine(Routine());

        IEnumerator Routine()
        {
            yield return new WaitForSeconds(0.5f);

            ThrowWeapon(longRange);
            ThrowWeapon(melle);

            yield return new WaitForSeconds(0.93f);

            TakeWeapon(melle);

            yield return new WaitForSeconds(0.1f);

            TakeWeapon(longRange);
        }
    }

    public void Stun()
    {
        ragdoll.Stun();
    }

    private void CalcultePower()
    {
        var bodyes = GetComponentsInChildren<Rigidbody2D>();
        foreach (var body in bodyes)
        {
            totalPower += Mathf.FloorToInt(body.mass);
        }
    }

    public class ArmJointSet
    {
        public Rigidbody2D arm;
        public Transform weaponHolder;
        public Transform hand;
    }
}


