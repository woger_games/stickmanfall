﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using System;
using TMPro;

using Photon.Pun;


public class UI : MonoBehaviour
{
    [SerializeField]
    private Button btnTake;
    [SerializeField]
    private Button btnThrow;
    [SerializeField]
    private Button btnJump;
    [SerializeField]
    private TMP_Text labelPing;

    [Space]

    [SerializeField]
    private HUD hud;

    Player playerMine;
    Action takeWeapon;
    Action throwWeapon;

    private void Awake()
    {
        hud.Init();
    }

    private void Start()
    {
        btnTake.gameObject.SetActive(false);
        btnThrow.gameObject.SetActive(false);

        btnTake.onClick.AddListener(() => takeWeapon?.Invoke());
        btnThrow.onClick.AddListener(() => throwWeapon?.Invoke());
        btnJump.onClick.AddListener(() => EventsHolder.jumpClicked?.Invoke());

        EventsHolder.playerSpawnedMine.AddListener(MinePLayer_Spawned);
        EventsHolder.weaponTriggered.AddListener(WeaponTriggered);
        EventsHolder.weaponTriggerExit.AddListener(WeaponTriggerExit);
        EventsHolder.weaponPicked.AddListener(Weapon_Picked);
        EventsHolder.weaponThrowed.AddListener(Weapon_Throwed);

        
    }

    private void Update()
    {
        var clientType = PhotonNetwork.IsMasterClient ? "Host" : "Client";
        labelPing.text = $"{clientType} | Ping {PhotonNetwork.GetPing()}";
    }

    void MinePLayer_Spawned(Player player) => playerMine = player;
    
    void WeaponTriggerExit(Player player, Weapon weapon)
    {
        btnTake.gameObject.SetActive(false);
    }

    private void WeaponTriggered(Player player, Weapon weapon)
    {
        if (playerMine == player)
        {
            btnTake.gameObject.SetActive(true);
            takeWeapon = () => playerMine.TakeWeapon(weapon);
        }
    }

    private void Weapon_Picked(Player player, Weapon weapon)
    {
        if (playerMine == player)
        {
            throwWeapon = () =>
            {
                var length = playerMine.Weapons.Count;
                for (int i = 0; i < length; i++)
                    playerMine.ThrowWeapon(playerMine.Weapons.First());
                playerMine.WeaponSet = WeaponSet.None;
            };//playerMine.ThrowWeapon(weapon);
            btnThrow.gameObject.SetActive(true);
        }
    }

    private void Weapon_Throwed(Player player, Weapon weapon)
    {
        if (playerMine == player)
        {
            btnThrow.gameObject.SetActive(false);
        }
    }
}
