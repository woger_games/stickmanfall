using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUD : MonoBehaviour
{
    [SerializeField]
    private ProgressBar progressBarPrefab;

    List<ProgressBarSet> playersBars;

    public void Init()
    {
        playersBars = new List<ProgressBarSet>();

        EventsHolder.playerSpawnedAny.AddListener(AnyPlayer_Spawned);
    }

    private void AnyPlayer_Spawned(Player player)
    {
        var bar = Instantiate(progressBarPrefab, transform);

        var pbs = new ProgressBarSet
        {
            player = player,
            bar = bar,
            health = player.GetComponent<HealthComponent>()
        };

        pbs.health.valueChanged += Health_Changed;

        playersBars.Add(pbs);
    }

    private void Health_Changed(HealthComponent health)
    {
        var barSet = playersBars.Find(s => s.health == health);

        var barValue = health.Value / health.MaxValue;

        barSet.bar.value = barValue;
    }

    private void Update()
    {
        foreach (var item in playersBars)
        {
            var p = item.player;
            var b = item.bar;

            var playerPos = new Vector2(p.Hip.position.x, p.Hip.position.y);

            var uiTransform = b.transform as RectTransform;
            var screenPoint = Camera.main.WorldToScreenPoint(playerPos + new Vector2(0, 2.1f));

            screenPoint.x /= transform.root.lossyScale.x;
            screenPoint.y /= transform.root.lossyScale.y;
            screenPoint.x -= uiTransform.sizeDelta.x / 2;

            uiTransform.anchoredPosition = Vector2.Lerp(uiTransform.anchoredPosition, screenPoint, 0.5f);

        }
    }

    public class ProgressBarSet
    {
        public Player player;
        public ProgressBar bar;
        public HealthComponent health;
    }
}
