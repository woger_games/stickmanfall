﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileInput : MonoBehaviour
{
    [SerializeField]
    private Joystick joystickLeft;

    [SerializeField]
    private Joystick joystickRight;

    [Header("*DEV*")]

    [SerializeField]
    TMPro.TMP_Text RightJoystickValue;


    Vector2 rightJoystickValue;

    void Update()
    {
        if(joystickLeft.Direction != Vector2.zero)
        {
            EventsHolder.leftJoystickMoved?.Invoke(joystickLeft.Direction);
        }

        if (joystickRight.Direction != Vector2.zero)
        {
            rightJoystickValue = joystickRight.Direction;

            EventsHolder.rightJoystickMoved?.Invoke(joystickRight.Direction);

            var a1 = VectorToSignedAngle(joystickRight.Direction);
            RightJoystickValue.text = $"{a1:F1}";
        }
        else if(rightJoystickValue != joystickRight.Direction)
        {
            rightJoystickValue = joystickRight.Direction;
            EventsHolder.rightJoystickUp?.Invoke();

            RightJoystickValue.text = "Нэ нажат";
        }

        
    }

    public static float VectorToSignedAngle(Vector2 dir)
    {
        dir.y *= -1f;
        float angleTarget = Vector2.SignedAngle(dir, Vector2.up);

        return angleTarget;
    }

    public static float VectorToAngle(Vector2 dir)
    {
        dir.y *= -1f;
        float angleTarget = Vector2.Angle(dir, Vector2.up);

        return angleTarget;
    }
}
