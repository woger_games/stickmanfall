﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class EventsHolder
{

    public class PlayerSpawned : UnityEvent<Player> { }

    public static PlayerSpawned playerSpawnedMine = new PlayerSpawned();

    //-----------------------------------------------------------------------

    public class EnemyDestroyed : UnityEvent<Transform, int> { }

    public static EnemyDestroyed enemyDestroyed = new EnemyDestroyed();

    //-----------------------------------------------------------------------

    public class PlayerSpawnedAny : UnityEvent<Player> { }

    public static PlayerSpawnedAny playerSpawnedAny = new PlayerSpawnedAny();

    //-----------------------------------------------------------------------

    public class LeftJoystickMoved : UnityEvent<Vector2> { }

    public static LeftJoystickMoved leftJoystickMoved = new LeftJoystickMoved();

    //-----------------------------------------------------------------------

    public class RightJoystickMoved : UnityEvent<Vector2> { }

    public static RightJoystickMoved rightJoystickMoved = new RightJoystickMoved();

    //-----------------------------------------------------------------------

    public class RightJoystickUp : UnityEvent { }

    public static RightJoystickUp rightJoystickUp = new RightJoystickUp();

    //-----------------------------------------------------------------------

    public class JumpClicked : UnityEvent { }

    public static JumpClicked jumpClicked = new JumpClicked();

    //-----------------------------------------------------------------------

    public class WeaponPicked : UnityEvent<Player, Weapon> { }

    public static WeaponPicked weaponPicked = new WeaponPicked();

    //-----------------------------------------------------------------------

    public class WeaponThrowed : UnityEvent<Player, Weapon> { }

    public static WeaponThrowed weaponThrowed = new WeaponThrowed();

    //-----------------------------------------------------------------------

    public class WeaponTriggered : UnityEvent<Player, Weapon> { }

    public static WeaponTriggered weaponTriggered = new WeaponTriggered();

    //-----------------------------------------------------------------------

    public class WeaponTriggerExit : UnityEvent<Player, Weapon> { }

    public static WeaponTriggerExit weaponTriggerExit = new WeaponTriggerExit();

    //-----------------------------------------------------------------------

    public class NetworkBodyInited : UnityEvent<Rigidbody2D> { }

    public static NetworkBodyInited networkBodyInited = new NetworkBodyInited();

    //-----------------------------------------------------------------------

    public class NetworkProjectileInited : UnityEvent<Projectile> { }

    public static NetworkProjectileInited networkProjectileInited = new NetworkProjectileInited();

    //-----------------------------------------------------------------------

    public class WeaponSetInited : UnityEvent<WeaponSet> { }

    public static WeaponSetInited weaponSetInited = new WeaponSetInited();

    //-----------------------------------------------------------------------

}
