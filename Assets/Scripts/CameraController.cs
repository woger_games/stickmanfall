﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private CinemachineVirtualCameraBase cam;

    private void Awake()
    {
        EventsHolder.playerSpawnedMine.AddListener(Plyer_Spawned);
    }

    void Plyer_Spawned(Player target)
    {
        cam.Follow = target.Hip;
    }
}
