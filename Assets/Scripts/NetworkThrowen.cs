using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class NetworkThrowen : MonoBehaviourPunCallbacks
{
    Throwen weapon;

    private void Awake()
    {
        weapon = GetComponent<Throwen>();
    }

    private void Start()
    {
        weapon.onAttack += Attacked;
        weapon.ViewID = photonView.ViewID;

        photonView.RegisterMethod<DirectionNetworkData>(EventCode.AttackThrowen, Network_Attacked, true);
    }

    private void Network_Attacked(DirectionNetworkData dirData)
    {
        print(dirData.value);

        if (PhotonNetwork.IsMasterClient)
        {
            if (dirData.viewID == photonView.ViewID)
            {
                weapon.ThrowAttack(dirData.value);
            }
        }
    }

    private void Attacked(Vector2 dir)
    {
        var data = new DirectionNetworkData { value = dir, viewID = weapon.ViewID.Value };

        photonView.RaiseEventAll(EventCode.AttackThrowen, data);
    }
}
