using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;

public class NetworkHealthComponent : MonoBehaviourPunCallbacks
{
    HealthComponent health;

    private void Start()
    {
        health = GetComponent<HealthComponent>();

        health.valueChanged += Health_Changed;

        photonView.RegisterMethod<HealthPointNetworkData>(EventCode.HealthChanged, Network_HealthChanged, true);
    }

    private void Network_HealthChanged(HealthPointNetworkData healthNetwork)
    {
        if (PhotonNetwork.IsMasterClient)
            return;

        if(healthNetwork.viewId == photonView.ViewID)
        {
            health.Value = healthNetwork.healthPoint;
        }
    }

    private void Health_Changed(HealthComponent health)
    {
        if (!PhotonNetwork.IsMasterClient)
            return;

        var data = new HealthPointNetworkData
        {
            healthPoint = (int)health.Value,
            viewId = photonView.ViewID
        };
        photonView.RaiseEventAll(EventCode.HealthChanged, data);
    }
}
