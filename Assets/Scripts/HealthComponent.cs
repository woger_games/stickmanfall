using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HealthComponent : MonoBehaviour
{
    [SerializeField] float maxHeath = 100;
    public float MaxValue => maxHeath;

    public Action<HealthComponent> valueChanged;

    public float Value 
    {
        get => value; 
        set
        {
            this.value = value;
            valueChanged?.Invoke(this);
        } 
    }

    float value;

    private void Start()
    {
        Value = maxHeath;
    }


}
