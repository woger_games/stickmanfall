﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee : Weapon
{
    [Header("= Melee =")]

    [SerializeField]
    private float damage = 5;

    Action Rotation;
    Transform weaponHolder;

    float hitResetTimer;

    protected override void Update()
    {
        Rotation?.Invoke();

        base.Update();

        hitResetTimer += Time.deltaTime;
    }

    public override void SetOwner(Player player)
    {
        base.SetOwner(player);

        weaponHolder = player.AvailableArm.weaponHolder;

        //Rotation = () => RestRotation = animationHandler.RightWeapon.rotation.eulerAngles.z;
        Rotation = () => RestRotation = weaponHolder.rotation.eulerAngles.z;
    }

        
    public override void BreakJoints()
    {
        base.BreakJoints();

        Rotation = null;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!MultiplayerManager.IsMaster)
            return;

        var magnitude = collision.relativeVelocity.magnitude;

        if (magnitude <= 19)
            return;

        if(collision.gameObject.layer == Constants.LAYER_PLAYER)
        {
            if (hitResetTimer > 0.5f)
            {
                hitResetTimer = 0;

                var health = collision.gameObject.transform.root.GetComponent<HealthComponent>();

                health.Value -= damage;
            }
        }
        
    }
}
