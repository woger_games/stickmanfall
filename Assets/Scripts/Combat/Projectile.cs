﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour, IDamage
{
    [SerializeField]
    float damageValue = 1f;
    public int OwnerID { get; set; }
    public int? ID { get; set; }
    public float Lifetime { get; set; }

    private Rigidbody2D body;

    public Rigidbody2D Body => body;

    public float DamageValue { get => damageValue; set => damageValue = value; }

    List<HealthComponent> hitsObjects = new List<HealthComponent>();

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!MultiplayerManager.IsMaster)
            return;

        if (collision.relativeVelocity.sqrMagnitude < 80)
            return;

        if (collision.gameObject.layer == Constants.LAYER_PLAYER)
        {
            var health = collision.gameObject.transform.root.GetComponent<HealthComponent>();

            if (!hitsObjects.Contains(health))
            {
                hitsObjects.Add(health);

                health.Value -= DamageValue;
            }
        }
    }

    public void Init(Player owner)
    {
        body = GetComponent<Rigidbody2D>();
        var col = GetComponent<Collider2D>();

        foreach (var muscle in owner.Ragdoll.muscles)
        {
            Physics2D.IgnoreCollision(col, muscle.bone.GetComponent<Collider2D>());
        }

        EventsHolder.networkProjectileInited?.Invoke(this);

        Lifetime = 0;
    }

    private void Update()
    {
        Lifetime += Time.deltaTime;

        if(Lifetime > 1f && GameManager.DESTROY_BULLET)
        {
            Destroy(gameObject);
        }
    }

    public static int GenerateID() => Random.Range(10000, 100000);
}
