using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine;
using System.Linq;
using System;

public class NetworkRagdollHandler : MonoBehaviourPunCallbacks, IPunObservable
{

    List<StickmanController> ragdolls;

    List<Vector2> positions;
    List<float> rotations;

    [SerializeField]
    int forcePos = 30;
    [SerializeField]
    int forceRot = 30;

    private void Awake()
    {
        ragdolls = new List<StickmanController>();
        positions = new List<Vector2>();
        rotations = new List<float>();
    }

    private void Start()
    {
        EventsHolder.playerSpawnedAny.AddListener(Player_Spawned);
    }

    private void FixedUpdate()
    {
        if (positions.Count < ragdolls.Count * 11)
            return;

        int idx = 0;

        foreach (var ragdoll in ragdolls)
        {

            foreach (var muscle in ragdoll.muscles)
            {
                muscle.bone.MovePosition(Vector2.Lerp(muscle.bone.position, positions[idx], forcePos * Time.deltaTime));
                muscle.bone.MoveRotation(Mathf.LerpAngle(muscle.bone.rotation, rotations[idx], forceRot * Time.deltaTime));

                idx++;
            }
        }
    }

    private void Player_Spawned(Player player)
    {
        ragdolls.Add(player.GetComponentInChildren<StickmanController>());

        ragdolls = ragdolls.OrderBy(p => p.GetComponentInParent<PhotonView>().OwnerActorNr).ToList();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        positions.Clear();
        rotations.Clear();

        int countReceivedItem = 0;

        foreach (var ragdoll in ragdolls)
        {
            if (stream.IsWriting)
            {
                foreach (var muscle in ragdoll.muscles)
                {
                    stream.SendNext(muscle.bone.position);
                    stream.SendNext(muscle.bone.rotation);
                }
            }
            else
            {
                var c = stream.Count;

                if (c < ragdolls.Count * 22)
                    return;

                foreach (var muscle in ragdoll.muscles)
                {
                    if (c > countReceivedItem)
                    {
                        var pos = (Vector2)stream.ReceiveNext();
                        positions.Add(pos);
                        countReceivedItem++;
                        var rot = (float)stream.ReceiveNext();
                        rotations.Add(rot);
                        countReceivedItem++;
                    }
                }
            }
        }

    }
}
